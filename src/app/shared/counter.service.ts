export class CounterService {
  statusChangeAccount = 0;

  increment() {
    console.log('A new status change detected. Total changes: ', ++this.statusChangeAccount);
  }

}
